networking-bagpipe (21.0.0-2) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090439).

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Dec 2024 16:04:48 +0100

networking-bagpipe (21.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 02 Oct 2024 16:41:00 +0200

networking-bagpipe (21.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 21 Sep 2024 14:02:55 +0200

networking-bagpipe (21.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed Add_os_ken_app_to_EVPN_OVSDataplaneDriver.patch applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Tue, 17 Sep 2024 10:47:45 +0200

networking-bagpipe (20.0.1-5) unstable; urgency=medium

  * Add Add_os_ken_app_to_EVPN_OVSDataplaneDriver.patch.

 -- Thomas Goirand <zigo@debian.org>  Thu, 23 May 2024 16:10:07 +0200

networking-bagpipe (20.0.1-4) unstable; urgency=medium

  * Removed nwdiag depends.

 -- Thomas Goirand <zigo@debian.org>  Fri, 17 May 2024 13:56:29 +0200

networking-bagpipe (20.0.1-3) unstable; urgency=medium

  * Restrict tests to Architecture: amd64 arm64 ppc64el s390x.

 -- Thomas Goirand <zigo@debian.org>  Mon, 06 May 2024 16:19:44 +0200

networking-bagpipe (20.0.1-2) unstable; urgency=medium

  * Remove extraneous python3-mock build dependency (Closes: #1066061).

 -- Thomas Goirand <zigo@debian.org>  Thu, 18 Apr 2024 09:09:47 +0200

networking-bagpipe (20.0.1-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 07 Apr 2024 22:02:01 +0200

networking-bagpipe (20.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * (build-)depends on neutron 2:24.0.0~.
  * Removed double python3-pbr build-depends.
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Sun, 17 Mar 2024 17:42:49 +0100

networking-bagpipe (19.0.0-2) unstable; urgency=medium

  * Add a new networking-bagpipe-bgp-agent package.

 -- Thomas Goirand <zigo@debian.org>  Mon, 11 Mar 2024 08:53:37 +0100

networking-bagpipe (19.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 05 Oct 2023 12:43:44 +0200

networking-bagpipe (19.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 15 Sep 2023 14:19:07 +0200

networking-bagpipe (18.0.0-2) unstable; urgency=medium

  * Cleans better (Closes: #1048765).

 -- Thomas Goirand <zigo@debian.org>  Tue, 22 Aug 2023 11:43:47 +0200

networking-bagpipe (18.0.0-1) unstable; urgency=medium

  * New usptream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Mar 2023 16:14:23 +0100

networking-bagpipe (17.0.0-3) unstable; urgency=medium

  * Remove autopkgtest, failing tests discovery.

 -- Thomas Goirand <zigo@debian.org>  Mon, 17 Oct 2022 13:36:02 +0200

networking-bagpipe (17.0.0-2) unstable; urgency=medium

  * autopkgtest: add 2 tests in the blacklist.

 -- Thomas Goirand <zigo@debian.org>  Fri, 14 Oct 2022 14:43:24 +0200

networking-bagpipe (17.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 05 Oct 2022 23:32:51 +0200

networking-bagpipe (17.0.0~rc1-1) unstable; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 18 Sep 2022 22:07:11 +0200

networking-bagpipe (16.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 30 Mar 2022 22:29:23 +0200

networking-bagpipe (16.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 25 Mar 2022 14:26:44 +0100

networking-bagpipe (16.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Sat, 12 Mar 2022 17:32:07 +0100

networking-bagpipe (15.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 06 Oct 2021 17:40:40 +0200

networking-bagpipe (15.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Sep 2021 13:25:49 +0200

networking-bagpipe (15.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 17 Sep 2021 11:31:34 +0200

networking-bagpipe (14.0.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 17:40:51 +0200

networking-bagpipe (14.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 14 Apr 2021 23:39:18 +0200

networking-bagpipe (14.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed (build-)depends version when satisfied in Bullseye.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 25 Mar 2021 13:48:02 +0100

networking-bagpipe (13.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Sun, 18 Oct 2020 14:42:23 +0200

networking-bagpipe (13.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Add python3-isort to build-depends.

 -- Thomas Goirand <zigo@debian.org>  Fri, 25 Sep 2020 09:39:45 +0200

networking-bagpipe (12.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 13 May 2020 16:51:31 +0200

networking-bagpipe (12.0.0~rc1-1) unstable; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Move the package to the neutron-plugins subgroup on Salsa.

 -- Thomas Goirand <zigo@debian.org>  Thu, 23 Apr 2020 11:46:20 +0200

networking-bagpipe (11.0.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Tue, 22 Oct 2019 23:35:17 +0200

networking-bagpipe (11.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 16 Oct 2019 19:56:32 +0200

networking-bagpipe (11.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Sat, 28 Sep 2019 23:59:50 +0200

networking-bagpipe (10.0.0-1) unstable; urgency=medium

  [ Thomas Goirand ]
  * New upstream release.
  * Removed package versions when satisfied in Buster.

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

 -- Thomas Goirand <zigo@debian.org>  Tue, 02 Apr 2019 14:27:04 +0200

networking-bagpipe (9.0.0-2) unstable; urgency=medium

  * override_dh_python3: dh_python3 --shebang=/usr/bin/python3.

 -- Thomas Goirand <zigo@debian.org>  Fri, 28 Dec 2018 16:31:11 +0100

networking-bagpipe (9.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 05 Sep 2018 18:40:39 +0200

networking-bagpipe (9.0.0~rc1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Removed remove-oslo_config.sphinxconfiggen.patch.

 -- Thomas Goirand <zigo@debian.org>  Tue, 28 Aug 2018 21:13:52 +0200

networking-bagpipe (8.0.0-2) unstable; urgency=medium

  * Add missing db migration files.

 -- Thomas Goirand <zigo@debian.org>  Tue, 19 Jun 2018 10:36:31 +0200

networking-bagpipe (8.0.0-1) unstable; urgency=medium

  * Initial release (Closes: #897648).

 -- Thomas Goirand <zigo@debian.org>  Thu, 03 May 2018 19:52:19 +0200
