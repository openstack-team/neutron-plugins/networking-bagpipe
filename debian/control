Source: networking-bagpipe
Section: net
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper-compat (= 11),
 dh-python,
 openstack-pkg-tools,
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 python3-babel,
 python3-coverage,
 python3-exabgp,
 python3-hacking,
 python3-isort,
 python3-netaddr,
 python3-networking-bgpvpn,
 python3-networking-sfc,
 python3-neutron (>= 2:24.0.0~),
 python3-neutron-lib,
 python3-openstackdocstheme,
 python3-os-testr,
 python3-oslo.concurrency,
 python3-oslo.config,
 python3-oslo.db,
 python3-oslo.i18n,
 python3-oslo.log,
 python3-oslo.messaging,
 python3-oslo.privsep,
 python3-oslo.rootwrap,
 python3-oslo.serialization,
 python3-oslo.service,
 python3-oslo.versionedobjects,
 python3-oslotest,
 python3-pecan,
 python3-pyroute2,
 python3-sphinxcontrib.actdiag,
 python3-sphinxcontrib.blockdiag,
 python3-sphinxcontrib.seqdiag,
 python3-stevedore,
 python3-subunit,
 python3-testresources,
 python3-testscenarios,
 python3-testtools,
 python3-webtest,
 subunit,
Standards-Version: 4.4.1
Homepage: http://github.com/openstack/networking-babpipe
Vcs-Browser: https://salsa.debian.org/openstack-team/neutron-plugins/networking-bagpipe
Vcs-Git: https://salsa.debian.org/openstack-team/neutron-plugins/networking-bagpipe.git

Package: networking-bagpipe-doc
Build-Profiles: <!nodoc>
Architecture: all
Section: doc
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: OpenStack virtual network service - BGP-based VPN - doc
 Neutron provides an API to dynamically request and configure virtual networks.
 These networks connect "interfaces" from other OpenStack services (such as
 vNICs from Nova VMs). The Neutron API supports extensions to provide advanced
 network capabilities, including QoS, ACLs, and network monitoring.
 .
 Driver and agent code to use BaGPipe lightweight implementation of BGP-based
 VPNs as a backend for Neutron.
 .
 This package contains the documentation.

Package: python3-networking-bagpipe
Architecture: all
Section: python
Depends:
 python3-babel,
 python3-exabgp,
 python3-netaddr,
 python3-networking-bgpvpn,
 python3-networking-sfc,
 python3-neutron (>= 2:24.0.0~),
 python3-neutron-lib,
 python3-oslo.concurrency,
 python3-oslo.config,
 python3-oslo.db,
 python3-oslo.i18n,
 python3-oslo.log,
 python3-oslo.messaging,
 python3-oslo.privsep,
 python3-oslo.rootwrap,
 python3-oslo.serialization,
 python3-oslo.service,
 python3-oslo.versionedobjects,
 python3-pbr,
 python3-pecan,
 python3-pyroute2,
 python3-sphinxcontrib.blockdiag,
 python3-sphinxcontrib.seqdiag,
 python3-stevedore,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 networking-bagpipe-doc,
Description: OpenStack virtual network service - BGP-based VPN - Python 3.x
 Neutron provides an API to dynamically request and configure virtual networks.
 These networks connect "interfaces" from other OpenStack services (such as
 vNICs from Nova VMs). The Neutron API supports extensions to provide advanced
 network capabilities, including QoS, ACLs, and network monitoring.
 .
 Driver and agent code to use BaGPipe lightweight implementation of BGP-based
 VPNs as a backend for Neutron.

Package: networking-bagpipe-bgp-agent
Architecture: all
Depends:
 python3-networking-bagpipe (= ${binary:Version}),
 ${misc:Depends},
 ${python3:Depends},
Breaks:
 python3-networking-bagpipe (<< 19.0.0-2~),
Replaces:
 python3-networking-bagpipe (<< 19.0.0-2~),
Description: OpenStack virtual network service - BGP-based VPN - agent
 Neutron provides an API to dynamically request and configure virtual networks.
 These networks connect "interfaces" from other OpenStack services (such as
 vNICs from Nova VMs). The Neutron API supports extensions to provide advanced
 network capabilities, including QoS, ACLs, and network monitoring.
 .
 This package contains the bgp agent, to be setup on each compute nodes.
